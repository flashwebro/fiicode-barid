# BarID

Prezentare video: https://youtu.be/gjmN_KS1bxQ

Aplicație web pentru scanarea codurilor de bare folosind camera device-ului sau o cameră web conectată. Rezultatele interpretate (numerice) ale scanărilor sunt salvate temporar și disponibile în panoul „Scan” al paginii web. Panoul „Result” se va deschide automat și va conține: detalii despre produsul identificat prin acel cod de bare - din baza de date MySQL dacă este înregistrat, sau din rezultatele oferite de [API-ul OpenFoodFacts](https://world.openfoodfacts.org/api/v0/product/737628064502.json).
  - Scanarea codurilor de bare direct cu camera device-ului prin API-ul HTML5 pentru media și biblioteca WebRTC. Cadrele din buffer sunt transmise local (în browser) la extensia [quaggaJS](https://serratus.github.io/quaggaJS/) pentru a fi procesate ca și cod de bare de biblioteca foarte cunoscută și robustă [zxing](https://github.com/zxing/zxing). În acest fel se asigură o citire rapidă, funcțională în mai multe poziții (codul de bare nu trebuie să fie perfect drept).
**Atenție: funcțiile media HTML5 cu acces la camera utilizatorului sunt disponibile numai când pagina client index.html și scripturile asociate script.js și webrtc.js sunt servite printr-o conexiune sigură (criptară) HTTPS, cel puțin în Chrome. Funcționează și cu un certificat self-signed, așa cum am folosit în video.**
Tot în pagina principală găsim și setări pentru numărul de thread-uri lansate pentru decodarea imaginilor, sampling, dimensiunea dreptunghiului în care se încearcă încadrarea codurilor detectate, etc.

![config](res/codetypes.png "Ecranul de configurări, pagina principală")


  - Bază de date MySQL, cu interfață de administrare (add/remove/edit produse și categorii) protejată prin autentificarea utilizatorilor (parolele sunt hash-uite MD5).

![admin](res/admin.png "Interfața de administrare")

![db-schema](res/scheme.png "Schema bazei de date")


  - Punerea la dispoziție a informațiilor către frontend-ul JavaScript+jQuery+AngularJS în formatul JSON: din baza de date, sau făcând passthrough la căutarea OpenFoodFacts.

![json](res/api.png "JSON API")

![off-passthrough](res/openfoodfacts.png "OpenFoodFacts lookup")


  - Posibilitatea de a scana UID-ul etichetelor RFID apropiate de un device mobil cu capacități NFC și căutarea lor în baza de date cu coduri de bare prin intermediul unui wrapper PhoneGap (Apache Cordova).


## Instalare
Mediu de deployment: server web LAMP/LEMP - Apache / Nginx + MySQL / MariaDB + conexiune SSL (HTTPS) obligatoriu.

Se importează baza de date db.sql prin linia de comandă sau PHPMyAdmin și se crează un user dedicat acesteia, cu acces numai din localhost dacă server-ul web și db sunt pe aceași mașină. Datele de conectare se actualizează în `dash/db.php` și `dash/preheader.php`.
Pentru a construi un APK wrapper cu capacitate de scanare RFID, editați în `RFIDapp/www/js/index.js` path-ul web la care se găsește API-ul (fișierul api.php). Faceți un zip cu tot conținutul RFIDapp și încărcați-l pe [Adobe Build](https://build.phonegap.com/). Alternativ, îl puteți construi folosind PhoneGap Desktop și un pachet Android ADT funcțional. Wrapper-ul arată și funcționează exact la fel ca aplicația web, dar la detectarea unei etichete RFID îi va căuta ID-ul prin API și va returna un popup cu rezultatele.

Login demo: address/dash - User: `admin` - Pass: `admin`

## Fișiere și foldere

| Path | Details |
| ------ | ------ |
| dist/css | Styles: Bootstrap, login... |
| dist/js/script.js | Cod funcțional scanare și API (local) lookup. |
| dist/js/* | Diverse biblioteci: Bootstrap, jQuery, AngularJS, Quagga, cod ajutător pentru editarea inline la vizualizarea CRUD. |
| index.html | Interfața publică a aplicației: scanner coduri de bare, panouri pentru setări, decodare, rezultatul căutării, etc. Conține și declarația META pentru capabilitatea de aplicație mobilă (fullscreen odată adăugată pe homescreen). |
| api.php | Cod funcțional - va returna JSON din rezultatele din baza de date pe baza codului primit de la scanner, în cazul lipsei unui rezultat va pasa query-ul la API-ul pentru coduri de bare al OpenFoodFacts și returnează rezultatul structurat pentru a fi înțeles de parser-ul Javascript. |
| dash/index.php | Edit/Add/Delete produse și categorii din baza de date. Interfață tabbed, se pot edita inline intrările. |
| dash/session.php, dash/login.php, dash/logout.php | Log-in, Log-out, sistemul de autentificare și verificare a unei sesiuni existente. |
| dash/db.php | Detaliile de conectare la baza de date, pentru API și login. |
| dash/ajaxCRUD.class.php, dash/preloader.php | Bibliotecă pentru a maniupla datele din MySQL sub formă de tabel cu editare inline. |
| RFIDapp | Fișierele necesare pentru construirea unei aplicații Android ca wrapper, pentru funcționalitate RFID. Momentan nu avem așa ceva ca API în browserele mobile, poate pe viitor :) |

### Credits
- Minea Claudiu Bogdan, clasa a XI-a, [Liceul Teoretic Internațional de Informatică București](http://liceu.ichb.ro/)
- Danciu Paul Tiberiu, clasa a XI-a, [Colegiul Național Grigore Moisil București](https://www.moisil.ro/)
    - & diferite biblioteci ♥ Open Source: Bootstrap, jQuery, AngularJS, QuaggaJS, zxing, AJAX CRUD, etc.