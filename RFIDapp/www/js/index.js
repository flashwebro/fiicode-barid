var scope; //pentru functia de apply in afara controllerului
var baseurl = 'https:// SERVER IP /barid/'; //adresa unde se afla api.php.. IP & path

function resmodal(){
    $('#resmodal').modal('toggle');
}

var app = {
    initialize: function() { // init
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false); // funcțiile device-ului se pot accesa prin Cordova
    },
    onDeviceReady: function() {
    	scope = angular.element($("#gets")).scope();
        document.getElementById('main').src=baseurl; 
    	nfc.addTagDiscoveredListener ( // cand este detectat un tag
        	function (nfcEvent) {
            	var id = nfc.bytesToHexString(nfcEvent.tag.id).toString(16); // convert
                $.ajax({
                    type       : "POST",
                    url        : baseurl+"/api.php",
                    crossDomain: true,
                    beforeSend : function() {  },
                    complete   : function() {  },
                    data       : { "code": id },
                    dataType   : 'json',
                    success    : function(response) {
                        
                    scope.$apply(function() {
                		scope.res = response;
            		}); 

                    resmodal();
                        
                    },
                    error      : function() {
                        alert('Error making request!');                  
                    } 
    }           ); 
        	},
        function () {
        },
        function (error) { // error callback
        	alert('NFC inactive or not available')
        }
    );
}
};
var myApp = angular.module('barid', []);
        myApp.controller('result', ['$scope', function ($scope) { // angular display
}]);