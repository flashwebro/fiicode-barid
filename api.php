<?php
include('dash/db.php');

$code = $mysqli->real_escape_string($_POST['code']); // sanitize

if ($stmt = $mysqli->prepare("SELECT title,categories.name,description FROM products INNER JOIN categories ON categories.id = products.cat_id WHERE code=?")) { // prepare statement
    $stmt->bind_param("s", $code); // substitute query var
    $stmt->execute(); // do query
    $stmt->bind_result($title,$cat,$desc); // bind properties
    $stmt->fetch(); // get values
    if($title) { $res=["title"=>$title,"cat"=>$cat,"desc"=>$desc]; echo json_encode($res); } // output local result
    else {
    	$data=json_decode(file_get_contents('https://world.openfoodfacts.org/api/v0/product/'.$code.'.json'));
			if($data->status){ // if product exists
    			$res=["title"=>$data->product->generic_name,"cat"=>$data->product->categories,"desc"=>$data->product->labels,"remote"=>1,"imgsrc"=>$data->product->image_front_url]; echo json_encode($res); // prepare and output remote result
			}
    }
    $stmt->close();
}

$mysqli->close();
?>