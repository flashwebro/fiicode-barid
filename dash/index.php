<?php
  include('session.php'); // sesiune
	require_once('preheader.php');
	include('ajaxCRUD.class.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<title>BarID Admin</title>
	<script type="text/javascript" src="../dist/js/jquery-compat.js"></script>
	<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
	<script type="text/javascript" src="../dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="../dist/js/crud.js"></script>
	<style type="text/css">
	body {
  		min-height: 2000px;
  		padding-top: 70px;
	}
	</style>
</head>
  <body>

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">BarID</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="./">Admin dash</a></li>
            <li><a href="../">Scanner app</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li><p class="navbar-text muted">Welcome, <?php echo $_SESSION['login_user']; ?></p></li>
              <li><a href="logout.php">Logout</a></li>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

 <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="./">Products</a></li>
    <li role="presentation"><a href="#cat" aria-controls="cat" role="tab" data-toggle="tab">Categories</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="prod">
    <?php
$tblDemo = new ajaxCRUD("Item", "products", "", "./");
$tblDemo->displayAs("code", "Barcode");
$tblDemo->displayAs("cat_id", "Category");
$tblDemo->displayAs("title", "Name");
$tblDemo->displayAs("description", "Description");
$tblDemo->addAjaxFilterBoxAllFields();
$tblDemo->defineRelationship("cat_id", "categories", "id", "name");
$tblDemo->showTable();
?></div>
    <div role="tabpanel" class="tab-pane" id="cat"><?php
$tblDemo = new ajaxCRUD("Category", "categories", "id", "./");
$tblDemo->displayAs("id", "#");
$tblDemo->displayAs("name", "Name");
$tblDemo->showTable();
?></div>
  </div>

    </div>

</body>
<script type="text/javascript">
$('table').addClass('table');
$('body').bind("DOMSubtreeModified",function(){
  $('table').addClass('table');
});
</script>
</html>