<?php
   include("db.php"); // conexiune SQL
   session_start(); // sesiune
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      $myusername = $_POST['username'];
      $mypassword = md5($_POST['password']); // hash parola
      $sql = "SELECT id FROM login WHERE username = '$myusername' and password = '$mypassword'";
      $result = $mysqli->query($sql);
      $row = $result->fetch_array();
      $count = $result->num_rows;		
      if($count == 1) {
         $_SESSION['login_user'] = $myusername; // logged in
         header("location: index.php");
      }else {
         $error = "Username sau parolă incorecte."; // return error
      }
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <title>TransitKIT Dash - Login</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width">
      <link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/login.css">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   </head>
   <body>
	
<div class="text-center" style="padding:50px 0"><center>
   <div class="logo">BarID</div>
   <!-- Login Form -->
   <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
   <div class="login-form-1">
      <form id="login-form" class="text-left" action="" method="POST">
         <div class="login-form-main-message"></div>
         <div class="main-login-form">
            <div class="login-group">
               <div class="form-group">
                  <label for="lg_username" class="sr-only">Username</label>
                  <input type="text" class="form-control" id="lg_username" name="username" placeholder="username" autofocus>
               </div>
               <div class="form-group">
                  <label for="lg_password" class="sr-only">Password</label>
                  <input type="password" class="form-control" id="lg_password" name="password" placeholder="password">
               </div>
            </div>
            <button type="submit" class="login-button" value="Submit"><i class="fa fa-chevron-right"></i></button>
         </div>
      </form>
   </div>
   <!-- Login Form --></center>
</div>

   </body>
</html>